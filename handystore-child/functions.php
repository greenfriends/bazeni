<?php
/* PlumTree functions and definitions */
add_action( 'wp_enqueue_scripts', 'handystore_child_enqueue_styles' );



function handystore_child_enqueue_styles() {
 
    $parent_style = 'parent-style';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style, 'plumtree-woo-styles' ),
        wp_get_theme()->get('Version')
    );
}

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Page Slider',
    'before_widget' => '<div class = "widgetizedArea">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);
function pt_store_title(){
  if ( (is_shop() || is_product_category() || is_product_tag()) && !is_front_page() ) {
     ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="page-title">
        <?php echo single_cat_title() ?>
      </div>
    </div> 
    <?php
  }
}
 add_action( 'init', 'pt_store_title', 20 );

 function pt_custom_breadcrumbs() {
  return array(
    'delimiter' => '<span> &#47; </span>',
    'wrap_before' => '<div class="col-md-12 col-sm-12 col-xs-12"><nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
    'wrap_after' => '</nav></div>',
    'before' => '',
    'after' => '',
    'home' => _x( 'Home', 'breadcrumb', 'handystore' ),
  );
}

add_action( 'init', 'pt_custom_breadcrumbs', 20 );

// Adding view all Link
add_action( 'woocommerce_before_shop_loop', 'pt_view_all_link', 25 );
if ( ! function_exists( 'pt_view_all_link' ) ) {
  function pt_view_all_link(){
    global $wp_query;
    $paged    = max( 1, $wp_query->get( 'paged' ) );
    $per_page = $wp_query->get( 'posts_per_page' );
    $total    = $wp_query->found_posts;
    $first    = ( $per_page * $paged ) - $per_page + 1;
    $last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );

    /* Get vendor store pages */
    $vendor_shop = '';
    if ( class_exists('WCV_Vendors') ) {
      $vendor_shop = urldecode( get_query_var( 'vendor_shop' ) );
    }

    if ( !is_search() && $wp_query->max_num_pages > 1 && $vendor_shop == '' ) { ?>
      <a rel="nofollow" class="view-all" href="?showall=1"><?php _e('Prikaži više', 'handystore'); ?></a>
    <?php }
    if( isset( $_GET['showall'] ) ){
      $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) ); ?>
        <a rel="nofollow" class="view-all" href="<?php echo esc_url($shop_page_url); ?>"><?php _e('Prikaži manje', 'handystore'); ?></a>
    <?php }
  }
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);